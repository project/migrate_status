Migrate status
==============

Need to determine if a migration is running? This module offers a service that
you can call to check that. Like this:

```
/**
 * Implements hook_entity_presave().
 */
function mymodule_entity_presave(EntityInterface $entity
) {
  /** @var \Drupal\migrate_status\MigrateStatus $migrate_status */
  $migrate_status = \Drupal::service('migrate.status');
  if ($migrate_status->isImporting()) {
    return;
  }

  // Migrate is not processing migrations. We can proceed to do something.
}
```

You can also check for a migration in particular via
<code>$migrate_status->isMigrationImporting('upgrade_d7_short_code_linkit')</code>
