<?php

namespace Drupal\migrate_status;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;

/**
 * Determines if a migration is executing, or has executed in this request.
 */
class MigrateStatus {

  /**
   * The migrate plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  private $manager;

  /**
   * A flag to mark that a migration is running.
   *
   * @var bool
   */
  private $importing = FALSE;

  /**
   * MigrationStatus constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $manager
   *   The migrate plugin service.
   */
  public function __construct(MigrationPluginManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Checks if there is any migration running.
   *
   * @return bool
   *   TRUE if there is at least one migration running. FALSE otherwise.
   */
  public function isImporting(): bool {
    // If it's a web request we can't be migrating.
    if (php_sapi_name() != 'cli') {
      return FALSE;
    }

    // Once a migration has been detected, keep the flag for the rest of the
    // request. Otherwise, if we were previously not importing, we could now
    // be in the middle of an operation, so check the migration statuses again.
    if (!$this->importing) {
      $definitions = $this->manager->getDefinitions();
      foreach ($definitions as $id => $definition) {
        /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
        $migration = $this->manager->createInstance($id);
        if ($this->checkStatus($migration)) {
          $this->importing = TRUE;
          break;
        }
      }
    }

    return $this->importing;
  }

  /**
   * Checks if a single migration is running.
   *
   * @param string $migration_id
   *   The identifier of a migration.
   *
   * @return bool
   *   TRUE if the migration is running. FALSE otherwise.
   */
  public function isMigrationImporting(string $migration_id): bool {
    // If it's a web request we can't be migrating.
    if (php_sapi_name() != 'cli') {
      return FALSE;
    }

    $migration = $this->manager->createInstance($migration_id);

    return $this->checkStatus($migration);
  }

  /**
   * Helper function to check for the status of a migration.
   *
   * @return bool
   *   TRUE if the migration is running. FALSE otherwise.
   */
  protected function checkStatus(MigrationInterface $migration) {
    return in_array($migration->getStatus(), [MigrationInterface::STATUS_IMPORTING, MigrationInterface::STATUS_STOPPING]);
  }

}
